package com.cusca.security.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "dashboard")
public class Welcome {

    @GetMapping
    public String dashboard(){
        return "Dashboard usuario logueado";
    }
}
