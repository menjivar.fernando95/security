package com.cusca.security.api;

import com.cusca.security.TokenService;
import com.cusca.security.dto.JwtDTO;
import com.cusca.security.dto.LoginDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@CrossOrigin( allowedHeaders = "*", originPatterns = "*", origins = "*")
@RequestMapping(path = "auth")
public class Home {

    private static final Logger LOG = LoggerFactory.getLogger(Home.class);


    private final TokenService tokenService;
    private final AuthenticationManager authenticationManager;


    public Home(TokenService tokenService, AuthenticationManager authenticationManager) {
        this.tokenService = tokenService;
        this.authenticationManager = authenticationManager;
    }


    @GetMapping
    public String home(Principal principal) {
        return "Hello, " + principal.getName();
    }

    @PostMapping("/token")
    public String token(Authentication authentication) {
        LOG.debug("Token requested for user: '{}'", authentication.getName());
        String token = tokenService.generateToken(authentication);
        LOG.debug("Token granted: {}", token);
        return token;
    }

    @PostMapping("/login")
    public ResponseEntity<JwtDTO> login(@RequestBody LoginDTO login, BindingResult bindingResult)throws AuthenticationException {
        if (bindingResult.hasErrors())
            return new ResponseEntity("Error", HttpStatus.BAD_REQUEST);

        Authentication authentication =
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(login.getUsername(),
                                login.getPassword()));
       // SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenService.generateToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        JwtDTO jwtDto = new JwtDTO("00",jwt, userDetails.getUsername());
        return new ResponseEntity<>(jwtDto, HttpStatus.OK);
    }
}
