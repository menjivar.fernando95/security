package com.cusca.security.dto;

import org.springframework.security.core.GrantedAuthority;

public class JwtDTO {

    private String code;
    private String token;
    private String bearer = "Bearer";
    private String username;


    public JwtDTO(String code,String token, String username) {
        this.code = code;
        this.token = token;
        this.username = username;

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBearer() {
        return bearer;
    }

    public void setBearer(String bearer) {
        this.bearer = bearer;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String nombreUsuario) {
        this.username = nombreUsuario;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
