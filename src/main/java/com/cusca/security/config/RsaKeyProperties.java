package com.cusca.security.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@ConfigurationProperties(prefix = "rsa")
public class RsaKeyProperties {
    private RSAPublicKey publicKey;
    private RSAPrivateKey privateKey;


    public RSAPublicKey publicKey() {
        return publicKey;
    }

    public RSAPrivateKey privateKey(){
        return privateKey;
    }
}
