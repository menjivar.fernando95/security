# Security server
Seguridad de los microservicios

## Features

- seguridad


## Installation
Importar el proyecto con intellijidea y aplicar correr el comando mvn install
Install the dependencies and devDependencies and start the server.

```sh
mvn install
```

Start...
Se puede correr desde el iDE o ejecutar el siguiente comando abriendo una consola en la carpeta target donde se guarda el compilado

```sh
java -jar security-0.0.1-SNAPSHOT.jar
```
### Se debe tener instalado java 11, y  se puede ver la instancia del microservicio corriendo en el puerto local http://localhost:8115

## License

MIT

**Free Software, Hell Yeah!**

